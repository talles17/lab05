/**
 * @file	empresa.h
 * @brief	Definiçao da classe Empresa e Funcionario para representar as empresas
 * @details	Os atributos da empresa sao nome da empresa, cnpj e funcionarios. Os do
 *			funcionario sao nome , salario e data de admissao.
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date	03/05/2017
*/
#ifndef EMPRESA_H
#define EMPRESA_H

#include <ostream>
using std::ostream ;
#include <istream>
using std::istream ;

#include <string>
using std::string ;

/**
 * @class 	empresa
 * @brief	classe que representa uma empressa	
 * @details	Os atributos da empresa sao nome da empresa, cnpj e funcionarios.
*/
class Empresa {
	private:
		string nome ;
		string cnpj ;
		int quantidade_funcionarios ;
		
	public:
		class Funcionario {
			private:
				string nome ;
				float salario ;
				string data_admissao ;

			public:
				/** @brief construtor padrao */
				Funcionario() ;
				/** @brief construtor parametrizado */
				Funcionario(string n , float s , string da) ;
				/** @brief retorna o nome do funcionario */
				string getNomeF() ;
				/** @brief modifica o nome do funcionario */
				void setNomeF (string n) ;
				/** @brief retorna o salario do funcionario */
				float getSalario() ;
				/** @brief modifica o salario do funcionario */
				void setSalario(float s) ;
				/** @brief retorna a data de admissao do funcionario */
				string getDataAdmissao() ;
				/** @brief modifica a data de admissao do funcionario */
				void setDataAdmissao(string da) ;
				//friend ostream& operator<<(ostream& os, Funcionario &f) ;
				//friend istream& operator>>(istream& is, Funcionario &f) ;
		}  ;
		Funcionario *f ;
		/** @brief construtor padrao */
		Empresa() ;
		/** @brief construtor parametrizado */
		Empresa(string n , string c , int qf , Funcionario* f ) ;
		/** @brief retorna o nome da empresa */
		string getNome() ;
		/** @brief modifica o nome da empresa */
		void setNome(string n) ;
		/** @brief retornar o cnpj da empresa */
		string getCNPJ() ;
		/** @brief modifica o cnpj da empresa */
		void setCNPJ(string c) ;
		/** @brief retornar a quantidade de funcionarios da empresa */
		int getQuantidadeDeFuncionarios() ;
		/** @brief modifica a quantidade de funcionarios da empresa */
		void setQuantidadeDeFuncionarios(int qf) ;
		Empresa& operator=(const Empresa e) ;
		friend ostream& operator<<(ostream& os, Empresa &e);
		friend istream& operator>>(istream& is, Empresa &e);

} ;

/**
 * @class 	funcionario
 * @brief	classe que representa um funcionario	
 * @details	Os do funcionario sao nome , salario e data de admissao.
*/



#endif /* EMPRESA_H */