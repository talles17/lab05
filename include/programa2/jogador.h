/**
 * @file	jogador.h
 * @brief	Definiçao da classe jogo, que representa um jogo de dados. 
 * @author	Cleydson Talles Araujo Vieira
 * @since	02/05/2017
 * @date	07/05/2017 
*/

#ifndef JOGADOR_H
#define JOGADOR_H

/**
 * @class 	Jogo jogador.h
 * @brief 	Classe que representa o jogo
*/
class Jogo {
	private:
		string id ; 
		int rodadas ;
		int dado1 , dado2 ;
		int soma_dados ;

	public:
		static int valor ;
		Jogo() ;
		Jogo(string i) ;
		Jogo(Jogo &s) ;

		/** @brief retorna nome do jogador */
		string getId() ;
		/** @brief modifica o nome do jogador */
		void setId(string i) ;
		/** @brief retorna o numero de rodadas */
		int getRodadas() ;
		/** @brief modifica o numero de rodadas */
		void setRodadas(int r) ;
		/** @brief retorna o numero do primeiro dado */
		int getDados1() ;
		/** @brief modifica o numero do primeiro dado */
		void setDados1(int d1) ;
		/** @brief retorna o numero do segundo dado */
		int getDados2() ;
		/** @brief modifica o numero do segundo dado */
		void setDados2(int d2) ;
		/** @brief retorna a soma dos dois dados */
		int getSomaDados() ;
		
		void jogada() ;
		void rodada() ;



} ;


#endif /* JOGADOR_H */