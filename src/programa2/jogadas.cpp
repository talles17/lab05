/**
 * @file	jogadas.cpp
 * @brief	Arquivo contendo os construtores do jogo e suas açoes
 * @author	Cleydson Talles Araujo Vieira
 * @since	02/05/2017
 * @date	07/05/2017 
 * @sa 		jogador.h
*/

#include <iostream>
using std::cout ;
using std::endl ;
using std::cin ;

#include <cstdlib>
using std::rand ;

#include <string>
using std::string ;

#include "jogador.h"

#define MAX_DADO 6

Jogo::Jogo () {

}
/**
 * @param i Nome do jogador
*/
Jogo::Jogo (string i) {	
	id = i ;
	rodadas = 0 ;
	dado1 = 0 ;
	dado2 = 0 ;
	soma_dados = 0 ;
}
/**
 * @param j Objeto da classe Jogo
*/
Jogo::Jogo(Jogo &j) {
	id = j.id;
	rodadas = j.rodadas ;
	dado1 = j.dado1 ;
	dado2 = j.dado2 ;
	soma_dados = j.soma_dados ;
}
/** @return nome do jogador */
string Jogo::getId() {
	return id ;
}
/** @param nome do jogador */
void Jogo::setId(string i) {
	id = i ;
}

/** @return a quantidade de rodadas */
int Jogo::getRodadas() {
	return rodadas ;
}
/** @param a quantidade de rodadas */
void Jogo::setRodadas(int r) {
	rodadas = r ;
}
/** @return numero do primeiro dado */
int Jogo::getDados1() {
	return dado1 ;
}
/** @param numero do primeiro dado */
void Jogo::setDados1(int d1) {
	dado1 = d1 ;
}
/** @return numero do segundo dado */
int Jogo::getDados2() {
	return dado1 ;
}
/** @param numero do segundo dado */
void Jogo::setDados2(int d2) {
	dado2 = d2 ;
}

/**
 * @details	Quando invocado, este metodo realiza o lançamento do primeiro dado, 
 *			e verifica se o jogador quer lançar o segundo dado, se sim ele faz
 *			o lançamento do segundo dado e faz a soma do mesmo com o primeiro
 *			dado.
*/
void Jogo::jogada() {
	dado1 = rand() % MAX_DADO + 1 ;
	cout <<"Dado1: " << dado1 << endl ;
	int s ;
	cout << "Digite 1 para jogar o segundo dado, ou 0 para sair da rodada. " ;
	cin >> s ;
	if (s == 1) {
		dado2 = rand() % MAX_DADO + 1 ;
		cout <<"Dado2: " << dado2 << endl ;
	} else dado2 = 0 ;
	
	soma_dados = dado1 + dado2 ;

}
/** @return numero de rodadas */
int Jogo::getSomaDados() {
	return soma_dados ;
}
/** @details numero de rodadas */
void Jogo::rodada() {
	rodadas++ ;
}
