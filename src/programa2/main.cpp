/**
 * @file	main.cpp
 * @brief	Codigo fonte principal do programa
 * @author	Cleydson Talles Araujo Vieira
 * @since	02/05/2017
 * @date	07/05/2017 
*/
#include <iostream>
using std::cout ;
using std::endl ;

#include <string>
using std::string ;

#include <ctime>
using std::time ;

#include <cstdlib>
using std::srand ;


#include "jogador.h"



int Jogo::valor = rand() % 12 + 1 ; 	

/** @brief Funcao principal */
int main () {
	
	srand(time(NULL)) ;

	
	Jogo jogador1("Lucas") ;
	Jogo jogador2("Paulo") ;
	Jogo jogador3("Bruno") ;

	Jogo v ;

	cout << "Valor a ser chegado: " << jogador1.valor << endl ; 

	while(true) {
		 

		cout << "\nVez do jogador " << jogador1.getId() << endl ;
		jogador1.jogada() ;
		cout << "Soma dos dados de "<< jogador1.getId() << " : " << jogador1.getSomaDados() << endl ;
		jogador1.rodada() ;
		if (jogador1.getSomaDados() > Jogo::valor ) {	
			cout << "O jogador " << jogador1.getId()<< " foi excluido da rodada" << endl ;
			while ((jogador2.getSomaDados() < Jogo::valor && jogador3.getSomaDados() < Jogo::valor)||
					(jogador2.getSomaDados() == Jogo::valor && jogador3.getSomaDados() == Jogo::valor) ){
				
				cout << "\nVez do jogador " << jogador2.getId() << endl ;
				jogador2.jogada() ;
				cout << "Soma dos dados de "<< jogador2.getId() << " : " << jogador2.getSomaDados() << endl ;
				jogador2.rodada() ;

				cout << "\nVez do jogador " << jogador3.getId() << endl ;
				jogador3.jogada() ;
				cout << "Soma dos dados de "<< jogador3.getId() << " : " << jogador3.getSomaDados() << endl ;
				jogador3.rodada() ;

				if (jogador2.getSomaDados() > Jogo::valor && jogador3.getSomaDados() < Jogo::valor) {
					cout << "O jogador " << jogador2.getId()<< " foi excluido da rodada" << endl ;
					cout << "O jogador " << jogador3.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador2.getSomaDados() < Jogo::valor && jogador3.getSomaDados() > Jogo::valor) {
					cout << "O jogador " << jogador3.getId()<< " foi excluido da roda2a" << endl ;
					cout << "O jogador " << jogador2.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador2.getSomaDados() == Jogo::valor && jogador3.getSomaDados() != Jogo::valor ) {	
					v = Jogo(jogador2) ;
					break ;
				}
				if (jogador2.getSomaDados() != Jogo::valor && jogador3.getSomaDados() == Jogo::valor ) {	
					v = Jogo(jogador3) ;
					break ;
				}

			}
		}	


		
		cout << "\nVez do jogador " << jogador2.getId() << endl ;
		jogador2.jogada() ;
		cout << "Soma dos dados de "<< jogador2.getId() << " : " << jogador2.getSomaDados() << endl ;
		jogador2.rodada() ;
		if (jogador2.getSomaDados() > Jogo::valor ) {	
			cout << "O jogador " << jogador2.getId()<< " foi excluido da rodada" << endl ;
			while ((jogador3.getSomaDados() < Jogo::valor && jogador1.getSomaDados() < Jogo::valor)||
				   (jogador3.getSomaDados() == Jogo::valor && jogador1.getSomaDados() == Jogo::valor) ){
				
				cout << "\nVez do jogador " << jogador3.getId() << endl ;
				jogador3.jogada() ;
				cout << "Soma dos dados de "<< jogador3.getId() << " : " << jogador3.getSomaDados() << endl ;
				jogador3.rodada() ;

				cout << "\nVez do jogador " << jogador1.getId() << endl ;
				jogador1.jogada() ;
				cout << "Soma dos dados de "<< jogador1.getId() << " : " << jogador1.getSomaDados() << endl ;
				jogador1.rodada() ;

				if (jogador3.getSomaDados() > Jogo::valor && jogador1.getSomaDados() < Jogo::valor) {
					cout << "O jogador " << jogador3.getId()<< " foi excluido da rodada" << endl ;
					cout << "O jogador " << jogador1.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador3.getSomaDados() < Jogo::valor && jogador1.getSomaDados() > Jogo::valor) {
					cout << "O jogador " << jogador1.getId()<< " foi excluido da rodada" << endl ;
					cout << "O jogador " << jogador3.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador3.getSomaDados() == Jogo::valor && jogador1.getSomaDados() != Jogo::valor ) {	
					v = Jogo(jogador3) ;
					break ;
				}
				if (jogador3.getSomaDados() != Jogo::valor && jogador1.getSomaDados() == Jogo::valor ) {	
					v = Jogo(jogador1) ;
					break ;
				}
			}
		}	
		
		cout << "\nVez do jogador " << jogador3.getId() << endl ;
		jogador3.jogada() ;
		cout << "Soma dos dados de "<< jogador3.getId() << " : " << jogador3.getSomaDados() << endl ;
		jogador3.rodada() ;
		if (jogador3.getSomaDados() > Jogo::valor ) {	
			cout << "O jogador " << jogador3.getId()<< " foi excluido da rodada" << endl ;
			while ((jogador1.getSomaDados() < Jogo::valor && jogador2.getSomaDados() < Jogo::valor)||
				   (jogador1.getSomaDados() == Jogo::valor && jogador3.getSomaDados() == Jogo::valor) ){
				
				cout << "\nVez do jogador " << jogador1.getId() << endl ;
				jogador1.jogada() ;
				cout << "Soma dos dados de "<< jogador1.getId() << " : " << jogador1.getSomaDados() << endl ;
				jogador1.rodada() ;

				cout << "\nVez do jogador " << jogador2.getId() << endl ;
				jogador2.jogada() ;
				cout << "Soma dos dados de "<< jogador2.getId() << " : " << jogador2.getSomaDados() << endl ;
				jogador2.rodada() ;

				if (jogador1.getSomaDados() > Jogo::valor && jogador2.getSomaDados() < Jogo::valor) {
					cout << "O jogador " << jogador1.getId()<< " foi excluido da rodada" << endl ;
					cout << "O jogador " << jogador2.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador1.getSomaDados() < Jogo::valor && jogador2.getSomaDados() > Jogo::valor) {
					cout << "O jogador " << jogador2.getId()<< " foi excluido da roda2a" << endl ;
					cout << "O jogador " << jogador1.getId()<< " e o vencedor da rodada" << endl ;
					break ;
				}
				if (jogador1.getSomaDados() == Jogo::valor && jogador2.getSomaDados() != Jogo::valor ) {	
					v = Jogo(jogador1) ;
					break ;
				}
				if (jogador1.getSomaDados() != Jogo::valor && jogador2.getSomaDados() == Jogo::valor ) {	
					v = Jogo(jogador2) ;
					break ;
				}
			}
		}

		

		if (jogador1.getSomaDados() == Jogo::valor ) {	
			v = Jogo(jogador1) ;
			break ;
		}


		if (jogador2.getSomaDados() == Jogo::valor ) {	
			v = Jogo(jogador2) ;
			break ;
		}

		if (jogador3.getSomaDados() == Jogo::valor ) {	
			v = Jogo(jogador3) ;
			break ;
		}

	

	}

	cout << "O jogador vencedor foi: " << v.getId() << endl ;


	return 0 ;

}