/**
 * @file	empresa.cpp
 * @brief	implementaçao dos metodos da classe empresa.
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date	03/05/2017
 * @sa		empresa.h
*/
#include "empresa.h"
#include <string>
using std::string ;

#include <ostream>
using std::ostream ;

Empresa::Empresa() {

}

Empresa::Empresa(string n , string c , int qf , Funcionario *f ) {
	setNome(n) ;
	setCNPJ(c) ;
	setQuantidadeDeFuncionarios(qf) ;
	f = new Funcionario[qf] ;
	for (int i = 0 ; i < qf ; i++) {
		f[i] = Funcionario() ;
	}
}

string Empresa::getNome() {
	return nome ;
}

void Empresa::setNome(string n) {
	nome = n ;
}

string Empresa::getCNPJ() {
	return cnpj ;
}

void Empresa::setCNPJ(string c) {
	cnpj = c ;
}

int Empresa::getQuantidadeDeFuncionarios() {
	return quantidade_funcionarios ;
}

void Empresa::setQuantidadeDeFuncionarios(int qf) {
	quantidade_funcionarios = qf ;
}

/*void Empresa::adciona_funcionario (empresa funcionarios) {
	ifstream entrada ("/home/talles/bti/lp/lab05/data/dados.csv") ;
	entrada >> 
} */

Empresa& Empresa::operator=(const Empresa e) {
	nome = e.nome ;
	cnpj = e.cnpj ;
	quantidade_funcionarios = e.quantidade_funcionarios ;


	return *this ;
}

/*ostream& operator<<(ostream& os, Empresa &e) {
	os << e.nome << " " << e.cnpj << " " ;
	for (int i = 0 ; i < e.quantidade_funcionarios ; i++) {
		os << e.Empresa::f->nome << " " << e.Empresa::f->salario << " " << e.Empresa::f->data_admissao ;
	}
	return os ;
}

istream& operator>>(istream& is, Empresa &e) {
	is >> e.nome >> e.cnpj ;
	for (int i = 0 ; i < e.quantidade_funcionarios ; i++) {
		is >> e.Empresa::f->nome << " " >> e.Empresa::f->salario >> " " >> e.Empresa::f->data_admissao ;
	}
	return is ;
}
 */